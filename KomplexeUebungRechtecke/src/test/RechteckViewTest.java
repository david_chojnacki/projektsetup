package test;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Zeichenflaeche;

@SuppressWarnings("serial")
public class RechteckViewTest extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		new RechteckViewTest().run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}
			getContentPane().repaint();
		}

	}

	public RechteckViewTest() {
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.add(new Rechteck(330, 330, 50, 50));
		brc.add(new Rechteck(380, 380, 50, 50));
		brc.add(new Rechteck(440, 440, 50, 50));
		brc.add(new Rechteck(500, 500, 50, 50));
		brc.add(new Rechteck(560, 440, 50, 50));
		brc.add(new Rechteck(620, 380, 50, 50));
		brc.add(new Rechteck(680, 330, 50, 50));
		brc.add(new Rechteck(740, 270, 50, 50));
		brc.add(new Rechteck(800, 210, 50, 50));
		brc.add(new Rechteck(860, 150, 50, 50));
		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setVisible(true);
	}

}

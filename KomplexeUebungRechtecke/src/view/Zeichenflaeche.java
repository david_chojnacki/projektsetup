package view;

import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.Color;
import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel{
	private BunteRechteckeController controller;

	public Zeichenflaeche(BunteRechteckeController controller) {
		super();
		this.controller = controller;
	}
	
	Rechteck r = new Rechteck();
	
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);
		for (int i = 0; i < controller.getRechtecke().size(); i++) { 
			r = (Rechteck) controller.getRechtecke().get(i);
			int breite = r.getBreite();
			int hoehe = r.getHoehe();
			int x = r.getX();
			int y = r.getY();
			g.drawRect(x,y,breite,hoehe);
		}	
	}
}

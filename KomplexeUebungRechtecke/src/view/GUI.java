package view;

import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.BunteRechteckGUI;
import view.Zeichenflaeche;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI(BunteRechteckeController controller) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		JTextPane txtpnX = new JTextPane();
		txtpnX.setText("X");
		contentPane.add(txtpnX);

		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);

		JTextPane txtpnY = new JTextPane();
		txtpnY.setText("Y");
		contentPane.add(txtpnY);

		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JTextPane txtpnBreite = new JTextPane();
		txtpnBreite.setText("Breite");
		contentPane.add(txtpnBreite);

		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JTextPane txtpnHhe = new JTextPane();
		txtpnHhe.setText("H\u00F6he");
		contentPane.add(txtpnHhe);

		textField_3 = new JTextField();
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JButton btnSpeichern = new JButton("Speichern");
		contentPane.add(btnSpeichern);
		setVisible(true);
	}

	public GUI() {
		// TODO Auto-generated constructor stub
	}

//	protected void buttonSpeichernClicked() {
//	 int x = Integer.parseInt(txtpnX.getText());
//	 int y Integer.parseInt(txtpnY.getText());
//	 int laenge Integer.parseInt(txtpnBreite.getText());
//	 int hoehe Integer.parseInt(txtpnHhe.getText());
//	 Rechteck r = new Rechteck(x,y, laenge, hoehe);
//	 BunteRechteckeController.add(r);
// }
	
}


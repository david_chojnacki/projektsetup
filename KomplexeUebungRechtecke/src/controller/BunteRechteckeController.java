package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.GUI;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		super();
		rechtecke=new LinkedList();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}
	
	public List getRechtecke(){
		return rechtecke;
	
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufuegen() {
		// TODO Auto-generated method stub
	new GUI(this);	
	}
}
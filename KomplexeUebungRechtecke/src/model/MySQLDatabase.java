package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.mysql.jdbc.Statement;

public class MySQLDatabase {
	String driver = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://localhost/rechtecke";
	String user = "root";
	String password = "";

	public void rechteckEintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "INSERT INTO T_Rechtecke (x,y,breite, hoehe) VALUES " + "(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Rechteck> getAlleRechtecke() {
		List rechtecke = new LinkedList<Rechteck>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			con.close();
			while (rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breote");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(x, y, breite, hoehe);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rechtecke;

	}
}

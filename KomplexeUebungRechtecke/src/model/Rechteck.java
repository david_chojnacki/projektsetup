package model;

import java.util.Random;

import model.Punkt;

public class Rechteck {
	private Punkt p;
	private int breite;
	private int hoehe;
	
	public Rechteck() {
		super();
		p = new Punkt(0,0);
		this.setBreite(0);
		this.setHoehe(0);;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		p = new Punkt(x,y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(int x, int y) {
		if(((this.getX() <= x)&&(x <= (this.getX()+this.getBreite())))&&((this.getY() <= y)&&(y <= (this.getY()+this.getHoehe())))){
			return true;
		}
		return false;
	}
	
	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		if (this.enthaelt(rechteck.getX(), rechteck.getY())) {
			if (this.enthaelt(rechteck.getX()+rechteck.getBreite(), rechteck.getY()+rechteck.getHoehe())) {
				return true;
			}
		}
		return false;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random zahl = new Random();
		int x = zahl.nextInt(1200);
		int y = zahl.nextInt(1000);
		int breite = zahl.nextInt(1200-x);
		int hoehe = zahl.nextInt(1000-y);
		return new Rechteck(x,y,breite,hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
	
}